const addReducer = (state = { paymentDetails: [] }, action) => {
	switch (action.type) {
		case "ADD_PAYMENTS_BEGIN":
			return { ...state, loading: true, error: null };
		case "FETCH_PAYMENTS_BEGIN":
			return { ...state, loading: true, error: null };
		case "ADD_PAYMENTS_SUCCESS":
			return { ...state, loading: false };
		case "ADD_PAYMENTS_FAILURE":
			return { ...state, loading: false, error: action.payload };
		case "FETCH_PAYMENTS_SUCCESS":
			return { ...state, paymentDetails: action.payload, loading: false };
		case "FETCH_PAYMENTS_FAILURE":
			return { ...state, loading: false, error: action.payload };
		case "FIND_BY_ID_SUCCESS":
			return { ...state, paymentDetails: action.payload };
		case "FIND_BY_ID_FAILURE":
			return { ...state, error: action.payload };
		default:
			return state;
	}
};

export default addReducer;
