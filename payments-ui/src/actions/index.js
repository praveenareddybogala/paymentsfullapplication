import axios from "axios";
const addPaymentsBegin = () => {
	return { type: "ADD_PAYMENTS_BEGIN" };
};
const addPaymentsSuccess = () => {
	return { type: "ADD_PAYMENTS_SUCCESS" };
};
const addPaymentsError = (err) => {
	return { type: "ADD_PAYMENTS_FAILURE", payload: err };
};
export const addPaymentDetails = (data) => {
	return (dispatch, getState) => {
		dispatch(addPaymentsBegin());
		return axios.post("http://localhost:8088/api/payment/save", data).then(
			() => {
				dispatch(addPaymentsSuccess());
			},
			(err) => {
				dispatch(addPaymentsError(err));
			}
		);
	};
};
const fetchPaymentsBegin = () => {
	return { type: "FETCH_PAYMENTS_BEGIN" };
};
export const fetchPaymentsSuccess = (res) => {
	return { type: "FETCH_PAYMENTS_SUCCESS", payload: res };
};
export const fetchPaymentsFailure = (err) => {
	return { type: "FETCH_PAYMENTS_FAILURE", payload: err };
};
export const fetchPaymentDetails = () => {
	return (dispatch, getState) => {
		dispatch(fetchPaymentsBegin());
		return axios.get("http://localhost:8088/api/payment/all").then(
			(res) => {
				dispatch(fetchPaymentsSuccess(res));
			},
			(err) => {
				dispatch(fetchPaymentsFailure(err));
			}
		);
	};
};
const findByIdSuccess = (res) => {
	return { type: "FIND_BY_ID_SUCCESS", payload: res };
};
const findByIdFailure = (err) => {
	return { type: "FIND_BY_ID_FAILURE", payload: err };
};
export const findPaymentsById = (value) => {
	return (dispatch, getState) => {
		return axios
			.get(`http://localhost:8088/api/payment/findById/${value.id}`)
			.then(
				(res) => {
					dispatch(findByIdSuccess(res));
				},
				(err) => {
					dispatch(findByIdFailure(err));
				}
			);
	};
};
