import React from "react";
import { Provider } from "react-redux";
import { createStore, applyMiddleware } from "redux";
import ReactDOM from "react-dom";
import addReducer from "./reducer/reducer";
import App from "./components/App";
import thunkMiddleware from "redux-thunk";

const store = createStore(addReducer, applyMiddleware(thunkMiddleware));
ReactDOM.render(
	<Provider store={store}>
		<App />
	</Provider>,
	document.getElementById("root")
);
