import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { findPaymentsById } from "../actions";

const FindById = ({ fetchPayments, setFetchPayments }) => {
	const [value, setValue] = useState("");
	const dispatch = useDispatch();

	const findById = (e) => {
		e.preventDefault();
		dispatch(findPaymentsById({ id: value }));
	};

	return (
		<div>
			Find by ID
			<input
				type="number"
				value={value}
				onChange={(e) => setValue(e.target.value)}
			/>
			<button type="button" onClick={findById}>
				Find
			</button>
		</div>
	);
};
export default FindById;
