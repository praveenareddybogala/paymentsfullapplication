import React, { useState, useEffect } from "react";
import { useDispatch } from "react-redux";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Navbar from "./Navbar";
import AddPayments from "./AddPayments";
import ViewPayments from "./ViewPayments";
import { fetchPaymentDetails } from "../actions";

const App = () => {
	const dispatch = useDispatch();
	const [fetchPayments, setFetchPayments] = useState(false);
	useEffect(() => {
		dispatch(fetchPaymentDetails());
	}, [fetchPayments, dispatch]);
	return (
		<Router>
			<div className="app">
				<Switch>
					<Route exact path="/" component={Navbar}></Route>
					<Route path="/addPayments">
						<AddPayments
							fetchPayments={fetchPayments}
							setFetchPayments={setFetchPayments}
						/>
					</Route>
					<Route path="/viewPayments">
						<ViewPayments
							fetchPayments={fetchPayments}
							setFetchPayments={setFetchPayments}
						/>
					</Route>
				</Switch>
			</div>
		</Router>
	);
};

export default App;
