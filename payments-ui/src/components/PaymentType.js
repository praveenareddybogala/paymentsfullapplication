import React from "react";
const PaymentType = (props) => {
	return (
		<div>
			<h5>Type:</h5>
			{props.type}
		</div>
	);
};
export default PaymentType;
