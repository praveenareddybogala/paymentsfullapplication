import React from "react";
import { NavLink } from "react-router-dom";

const Navbar = () => {
	return (
		<div>
			<h1>Welcome to payments application</h1>
			<ul className="nav">
				<li className="nav-item">
					<NavLink to="/addPayments" className="nav-link text-secondary">
						Add Payments
					</NavLink>
				</li>
				<li className="nav-item">
					<NavLink to="/viewPayments" className="nav-link text-secondary">
						View Payments
					</NavLink>
				</li>
			</ul>
		</div>
	);
};
export default Navbar;
