import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link, useHistory } from "react-router-dom";
import { addPaymentDetails } from "../actions";
import "../Styling/message.css";

const AddPayments = ({ fetchPayments, setFetchPayments }) => {
	const dispatch = useDispatch();
	const history = useHistory();
	const [id, setId] = useState("");
	const [paymentDate, setDate] = useState("");
	const [type, setType] = useState("");
	const [amount, setAmount] = useState("");
	const [custId, setCustid] = useState("");
	const [custName, setCustName] = useState("");
	const error = useSelector((state) => state.error);
	const payments = useSelector((state) => state.paymentDetails.data);

	const successMessage = () => {
		if (error) {
			return (document.getElementById(
				"msg"
			).innerText = `Failed to add: ${error.message}`);
		} else if (payments) {
			return (document.getElementById("msg").innerText = "Successfully Added");
		}
	};

	const handleSubmit = async (e) => {
		e.preventDefault();
		dispatch(
			await addPaymentDetails({
				id: id,
				paymentDate: paymentDate,
				type: type,
				amount: amount,
				custId: custId,
				custName: custName,
			})
		)
			.then(() => {
				successMessage();
				setFetchPayments(!fetchPayments);
			})
			.catch(() => {});
		// .finally(() => {
		// 	history.push("/");
		// });
	};
	return (
		<div>
			<h1>Add Payments</h1>
			<form onSubmit={handleSubmit}>
				<table className="table">
					<tbody>
						<tr>
							<td>
								<label>ID:</label>
							</td>
							<td>
								<input
									required
									type="number"
									value={id}
									onChange={(e) => setId(e.target.value)}
								/>
							</td>
						</tr>
						<tr>
							<td>
								<label>Date:</label>
							</td>
							<td>
								<input
									required
									type="date"
									value={paymentDate}
									onChange={(e) => setDate(e.target.value)}
								/>
							</td>
						</tr>
						<tr>
							<td>
								<label>Type:</label>
							</td>
							<td>
								<input
									required
									type="text"
									value={type}
									onChange={(e) => setType(e.target.value)}
								/>
							</td>
						</tr>
						<tr>
							<td>
								<label>Amount:</label>
							</td>
							<td>
								<input
									required
									type="number"
									value={amount}
									onChange={(e) => setAmount(e.target.value)}
								/>
							</td>
						</tr>
						<tr>
							<td>
								<label>CustID:</label>
							</td>
							<td>
								<input
									required
									type="number"
									value={custId}
									onChange={(e) => setCustid(e.target.value)}
								/>
							</td>
						</tr>
						<tr>
							<td>
								<label>CustName:</label>
							</td>
							<td>
								<input
									required
									type="text"
									value={custName}
									onChange={(e) => setCustName(e.target.value)}
								/>
							</td>
						</tr>
						<tr>
							<td colSpan="2">
								<button type="submit">Submit</button>
							</td>
						</tr>
						<tr>
							<td colSpan="2">
								<span id="msg" className="message"></span>
							</td>
						</tr>
					</tbody>
				</table>
			</form>
			<Link to="/">Back</Link>
		</div>
	);
};
export default AddPayments;
