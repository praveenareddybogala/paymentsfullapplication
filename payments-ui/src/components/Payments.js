import React from "react";
import PaymentDate from "./PaymentDate";
import PaymentType from "./PaymentType";
import PaymentAmount from "./PaymentAmount";
import Custid from "./Custid";
import Id from "./Id";

const Payments = ({ id, type, date, amount, custid, custName }) => {
	return (
		<div className="col-md-4">
			<h4>{custName}</h4>
			<div className="card mb-4 box-shadow" style={{ width: "18rem" }}>
				<div className="card-body">
					<Id id={id} />
					<PaymentType type={type} />
					<PaymentDate date={date} />
					<PaymentAmount amount={amount} />
					<Custid custid={custid} />
				</div>
			</div>
		</div>
	);
};
export default Payments;
