//import React from "react";
import { useSelector } from "react-redux";
import Payments from "./Payments";
import { useHistory } from "react-router-dom";
import FindById from "./FindById";
const ViewPayments = ({ fetchPayments, setFetchPayments }) => {
	const history = useHistory();
	const payments = useSelector((state) => state.paymentDetails.data);
	const error = useSelector((state) => state.error);
	const loading = useSelector((state) => state.laoding);
	const handleOnClick = (e) => {
		setFetchPayments(!fetchPayments);
		history.push("/");
	};
	if (error) {
		return <div className="d-flex justify-content-center">{error.message}</div>;
	}
	if (loading) {
		return (
			<div classname="d-flex justify-content-center">
				<div
					className="spinner-border m-5"
					style={{ width: "4rem", height: "4rem" }}
					role="status"
				>
					<span className="sr-only">Loading...</span>
				</div>
			</div>
		);
	}
	if (payments.length > 1) {
		return (
			<div className="container">
				<button onClick={handleOnClick}>Back</button>
				<FindById
					fetchPayments={fetchPayments}
					setFetchPayments={setFetchPayments}
				/>
				<div className="row">
					{payments.map((details) => {
						return (
							<Payments
								key={details.id}
								id={details.id}
								type={details.type}
								date={details.paymentDate}
								amount={details.amount}
								custid={details.custId}
								custName={details.custName}
							/>
						);
					})}
				</div>
				<button onClick={handleOnClick}>Back</button>
			</div>
		);
	} else {
		return (
			<div className="container">
				<button onClick={handleOnClick}>Back</button>
				<FindById
					fetchPayments={fetchPayments}
					setFetchPayments={setFetchPayments}
				/>
				<div className="row">
					<Payments
						key={payments.id}
						id={payments.id}
						type={payments.type}
						date={payments.paymentDate}
						amount={payments.amount}
						custid={payments.custId}
						custName={payments.custName}
					/>
				</div>
				<button onClick={handleOnClick}>Back</button>
			</div>
		);
	}
};
export default ViewPayments;
