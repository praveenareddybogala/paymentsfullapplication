import React from "react";
const PaymentAmount = (props) => {
	return (
		<div>
			<h5>Amount:</h5>
			{props.amount}
		</div>
	);
};
export default PaymentAmount;
