import React from "react";
const PaymentDate = (props) => {
	return (
		<div>
			<h5>Date:</h5> {props.date}
		</div>
	);
};
export default PaymentDate;
